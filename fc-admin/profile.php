<?php require_once('loader.php'); ?>
<?php $sitename = "Thông tin thành viên | " . SITENAME ?>
<?php include('header.php') ?>
<?php
if ($FcSession->is_logged_in() == false) {
    Helper::redirect_to('login.php');
}
$db = &$DBO;
require_once('includes/models/tbl_user.php');
if (isset($_POST['submit'])) {
    $data = Helper::trimData($_POST);
    $data['user_name'] = null;
    $data['id'] = $_SESSION['id'];
    $data['date'] = $db->escape_value($data['date']);
    $data['fullname'] = $db->escape_value($data['fullname']);
    $data['display_name'] = $db->escape_value($data['display_name']);
    $data['user_sex'] = $db->escape_value($data['user_sex']);
    if (Helper::isNumeric($data['user_sex']) >= 0) {
        $ObUser = new Tbl_User($db);
        if ($ObUser->store($data)) {
            $ok = '<span style="color:blue;">Cập nhật thông thành công</span>';
        } else {
            $error_data = '<span style="color:red;">Cập nhật thông tin thất bại</span>';
        }
    }
}

$query = "SELECT * FROM `tbl_user` WHERE `id` = " . $_SESSION['id'] . ' LIMIT 1';
$db->setQuery($query);
$row = $db->loadObject();

if (isset($_POST['submit_pass'])) {
    $data = $_POST;
    if (md5($data['curent_user_pass']) == $row->user_pass && $data['newpass'] == $data['renewpass'] && strlen($data['newpass']) > 3) {
        $data['id'] = $_SESSION['id'];
        $data['user_pass'] = md5($db->escape_value(trim($data['newpass'])));
        $ObUser = new Tbl_User($db);
        if ($ObUser->store($data)) {
            $ok = '<span style="color:blue;">Cập nhật thông thành công</span>';
        } else {
            echo $db->_sql . "<br />";
            $error_data = '<span style="color:red;">Cập nhật thông tin thất bại vì một lý do nào đó. Vui lòng xem lại thông tin!</span>';
        }
    } else {
        $error_data = '<span style="color:red;">Cập nhật thông tin thất bại, vui lòng xem lại mật khẩu cũ và mật khẩu mới!</span>';
    }
}

?>
    <div class="content">
        <fieldset style="padding: 0;">
            <table style="width: 100%;">
                <tr>
                    <td><h2 id="categories-php" class="phplogo">Chỉnh sửa thông tin thành viên</h2></td>
                    <td align="right">&nbsp;
                    </td>
                </tr>
            </table>
        </fieldset>
        <?php if (isset($error_data) || isset($ok)) : ?>
            <fieldset>
                <table>
                    <tr>
                        <td colspan="2">
                            <ul>
                                <?php
                                if ($error_data != '') echo '<li>' . $error_data . '</li>';
                                if ($ok != '') echo '<li>' . $ok . '</li>';
                                ?>
                            </ul>
                        </td>

                    </tr>
                </table>
            </fieldset>
        <?php endif; ?>
        <form method="post" action="<?php echo FCPATH . 'fc-admin/profile.php' ?>" name="edit_infomation">
            <fieldset style="float: left; width: 45%; min-height: 200px;">
                <table width="100%">
                    <tr>
                        <td width="100">Tên đăng nhập<span style="color: red;"> * </span></td>
                        <td><input type="text" name="uname" style="width: 95%;" class="ftext"
                                   value="<?php echo $row->user_name; ?>" readonly="readonly"/></td>
                    </tr>

                    <tr>
                        <td width="100">Họ và tên</td>
                        <td><input style="width: 95%;" type="text" name="fullname" class="ftext"
                                   value="<?php echo $row->fullname; ?>"/></td>
                    </tr>
                    <tr>
                        <td width="100">Tên hiển thị</td>
                        <td><input style="width: 95%;" type="text" name="display_name" class="ftext"
                                   value="<?php echo $row->display_name; ?>"/></td>
                    </tr>
                    <tr>
                        <td width="100">Ngày sinh</td>
                        <td><input style="width: 95%;" type="text" name="date" class="ftext"
                                   value="<?php echo $row->date; ?>"/></td>
                    </tr>
                    <tr>
                        <td width="100">Giới tính</td>
                        <td style="padding: 5px;">
                            <label>Nam&nbsp;&nbsp;&nbsp; </label><input
                                    type="radio" <?php echo $row->user_sex ? 'checked="checked"' : ''; ?> value="1"
                                    name="user_sex"/>
                            <label>&nbsp; Nữ&nbsp;&nbsp;&nbsp;</label><input
                                    type="radio" <?php echo $row->user_sex ? '' : 'checked="checked"'; ?> value="0"
                                    name="user_sex"/>
                        </td>
                    </tr>
                    <tr>
                        <td width="100">Email <span style="color: red;"> * </span></td>
                        <td style="padding: 5px;">
                            <input type="text" value="<?php echo $row->user_email; ?>" name="email" readonly="readonly"
                                   style="width: 96%;" class="ftext"/>
                        </td>
                    </tr>
                    <tr>
                        <td align="center" colspan="2">Chú thích: <span style="color: red;"> * </span> Giá trị không
                            được thay đổi
                        </td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                        <td align="left">
                            <input type="submit" class="search" style="padding:5px; margin-left: 0; margin-top: 10px;"
                                   value="Đổi thông tin" name="submit"/>
                            <input type="hidden" name="id" value=""/>
                        </td>
                    </tr>
                </table>
            </fieldset>
        </form>
        <form method="post" action="<?php echo FCPATH . 'fc-admin/profile.php' ?>" name="editpass">
            <fieldset style="float: right; width: 45%;min-height: 200px;">
                <table width="100%">
                    <tr>
                        <td colspan="2"><input type="text" value="Đổi mật khẩu" name="doimatkhau" class="ftext"
                                               style="border: 0; background: none; color: #666666;"/></td>
                    </tr>
                    <tr>
                        <td width="100">Mật khẩu cũ</td>
                        <td><input type="password" name="curent_user_pass" style="width: 95%;" class="ftext"
                                   value="123456" onclick="value='';"/></td>
                    </tr>

                    <tr>
                        <td width="100">Mật khẩu mới</td>
                        <td><input style="width: 95%;" type="password" name="newpass" class="ftext" value=""/></td>
                    </tr>
                    <tr>
                        <td width="100">Xác nhận mật khẩu</td>
                        <td><input style="width: 95%;" type="password" name="renewpass" class="ftext" value=""/></td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                        <td align="left">
                            <input type="submit" class="search" style="padding:5px; margin-left: 0; margin-top: 10px;"
                                   value="Đổi thông tin" name="submit_pass"/>
                            <input type="hidden" name="id" value=""/>
                        </td>
                    </tr>
                </table>
            </fieldset>
        </form>
    </div>
    <div class="clear"></div>
<?php include('footer.php') ?>