<?php

class Helper
{

    //Kiem tra pass : 0->9;a->z;A->Z
    public static function checkPass($pass)
    {
        if (preg_match('/^([a-zA-Z0-9])+$/', $pass)) {
            return true;
        } else {
            return false;
        }
    }


    //Kiem tra ten dang nhap :0->9;a->z;A->Z  va "_"
    public static function checkUser($user)
    {
        if (preg_match('/^([a-zA-Z0-9_])+$/', $user)) {
            return true;
        } else {
            return false;
        }
    }

    //Kiem tra 1 chuoi co la so nguyen va >0 hay khong?
    public static function isNumeric($num)
    {
        if (is_numeric($num)) {
            $k = (int)$num;
            $k = intval($k);
            if (is_int($k) or $k == 0) return $k;
        } else {
            return false;
        }
    }

    public static function checkField($array1, $array2)
    {
        $array_result = array();
        for ($i = 0; $i < count($array1); $i++) {
            if (!isset($_POST[$array1[$i]]) || empty($_POST[$array1[$i]]))
                $array_result[] = $array2[$i];
        }
        return $array_result;
    }

    public static function redirect_to($location)
    {
        if ($location != NULL) {
            header("Location: {$location}");
            exit;
        }
    }

    public static function slugVN($string)
    {
        $trans = array(
            'à' => 'a', 'á' => 'a', 'ả' => 'a', 'ã' => 'a', 'ạ' => 'a',
            'ă' => 'a', 'ằ' => 'a', 'ắ' => 'a', 'ẳ' => 'a', 'ẵ' => 'a', 'ặ' => 'a',
            'â' => 'a', 'ầ' => 'a', 'ấ' => 'a', 'ẩ' => 'a', 'ẫ' => 'a', 'ậ' => 'a',
            'À' => 'a', 'Á' => 'a', 'Ả' => 'a', 'Ã' => 'a', 'Ạ' => 'a',
            'Ă' => 'a', 'Ằ' => 'a', 'Ắ' => 'a', 'Ẳ' => 'a', 'Ẵ' => 'a', 'Ặ' => 'a',
            'Â' => 'a', 'Ầ' => 'a', 'Ấ' => 'a', 'Ẩ' => 'a', 'Ẫ' => 'a', 'Ậ' => 'a',
            'đ' => 'd', 'Đ' => 'd',
            'è' => 'e', 'é' => 'e', 'ẻ' => 'e', 'ẽ' => 'e', 'ẹ' => 'e',
            'ê' => 'e', 'ề' => 'e', 'ế' => 'e', 'ể' => 'e', 'ễ' => 'e', 'ệ' => 'e',
            'È' => 'e', 'É' => 'e', 'Ẻ' => 'e', 'Ẽ' => 'e', 'Ẹ' => 'e',
            'Ê' => 'e', 'Ề' => 'e', 'Ế' => 'e', 'Ể' => 'e', 'Ễ' => 'e', 'Ệ' => 'e',
            'ì' => 'i', 'í' => 'i', 'ỉ' => 'i', 'ĩ' => 'i', 'ị' => 'i',
            'Ì' => 'i', 'Í' => 'i', 'Ỉ' => 'i', 'Ĩ' => 'i', 'Ị' => 'i',
            'ò' => 'o', 'ó' => 'o', 'ỏ' => 'o', 'õ' => 'o', 'ọ' => 'o',
            'ô' => 'o', 'ồ' => 'o', 'ố' => 'o', 'ổ' => 'o', 'ỗ' => 'o', 'ộ' => 'o',
            'ơ' => 'o', 'ờ' => 'o', 'ớ' => 'o', 'ở' => 'o', 'ỡ' => 'o', 'ợ' => 'o',
            'Ò' => 'o', 'Ó' => 'o', 'Ỏ' => 'o', 'Õ' => 'o', 'Ọ' => 'o',
            'Ô' => 'o', 'Ồ' => 'o', 'Ố' => 'o', 'Ổ' => 'o', 'Ỗ' => 'o', 'Ộ' => 'o',
            'Ơ' => 'o', 'Ờ' => 'o', 'Ớ' => 'o', 'Ở' => 'o', 'Ỡ' => 'o', 'Ợ' => 'o',
            'ù' => 'u', 'ú' => 'u', 'ủ' => 'u', 'ũ' => 'u', 'ụ' => 'u',
            'ư' => 'u', 'ừ' => 'u', 'ứ' => 'u', 'ử' => 'u', 'ữ' => 'u', 'ự' => 'u',
            'Ù' => 'u', 'Ú' => 'u', 'Ủ' => 'u', 'Ũ' => 'u', 'Ụ' => 'u',
            'Ư' => 'u', 'Ừ' => 'u', 'Ứ' => 'u', 'Ử' => 'u', 'Ữ' => 'u', 'Ự' => 'u',
            'ỳ' => 'y', 'ý' => 'y', 'ỷ' => 'y', 'ỹ' => 'y', 'ỵ' => 'y',
            'Y' => 'y', 'Ỳ' => 'y', 'Ý' => 'y', 'Ỷ' => 'y', 'Ỹ' => 'y', 'Ỵ' => 'y'
        );
        $string = str_replace(' ', '-', $string);
        $string = strtolower($string);
        return strtr($string, $trans);
    }

    public static function trimData($data)
    {
        foreach ($data as $dt => $value) {
            $data[$dt] = trim($data[$dt]);
        }
        return $data;
    }

    public static function Paging($path, $limitstart, $limit, $display, $params = '')
    {
        if ($params == '') {
            $params = null;
            $params = array();
        }
        $str_limitstart = '';
        if (count($params) > 0) {
            $str_limitstart = implode('&amp;', $params);
        }
        if ($str_limitstart != '')
            $str_limitstart = '?' . $str_limitstart . '&limitstart=';
        else $str_limitstart = '?limitstart=';
        $totalPage = ceil($limit / $display);
        $limitstart = Helper::isNumeric($limitstart);
        if ($limitstart <= 0 || $limitstart > $totalPage) $limitstart = 1;
        if ($totalPage < 5 && $totalPage > 1) {
            $string = '<ul class="paging"><li><span>Trang ' . $limitstart . ' /' . $totalPage . ' </span></li>';
            if ($limitstart > 1) {
                $string .= '<li><a href="' . $path . $str_limitstart . ($limitstart - 1) . '"><span>Trang trước</span></a></li>';
            }
            for ($i = 1; $i <= $totalPage; $i++) {
                if ($i == $limitstart)
                    $string .= '<li><span>' . $i . '</span></li>'; else $string .= '<li><a href="' . $path . $str_limitstart . $i . '"><span>' . $i . '</span></a></li>';
            }
            if ($limitstart < $totalPage) {
                $string .= '<li><a href="' . $path . $str_limitstart . ($limitstart + 1) . '"><span>Trang sau</span></a></li>';
            }
            $string .= '</ul>';
        } else {
            $string = '<ul class="paging"><li><span>Trang ' . $limitstart . ' /' . $totalPage . ' </span></li>';
            if ($limitstart > 1) {
                $string .= '<li><a href="' . $path . $str_limitstart . ($limitstart - 1) . '"><span>Trang trước</span></a></li>';
            }
            $tmp_string = '';
            if ($limitstart - 3 > 1) $tmp_string .= '<li><a href="' . $path . $str_limitstart . '1"><span>1</span></a></li>';
            if ($limitstart - 3 > 2) $tmp_string .= '<li><span class="dots">...</span><li>';
            for ($i = $limitstart - 3; $i < $limitstart; $i++) {
                if ($i > 0)
                    if ($i == $limitstart)
                        $tmp_string .= '<li><span>' . $i . '</span></li>'; else $tmp_string .= '<li><a href="' . $path . $str_limitstart . $i . '"><span>' . $i . '</span></a></li>';
            }

            for ($i = $limitstart; $i <= $limitstart + 3 && $i <= $totalPage; $i++) {
                if ($i == $limitstart)
                    $tmp_string .= '<li><span>' . $i . '</span></li>'; else $tmp_string .= '<li><a href="' . $path . $str_limitstart . $i . '"><span>' . $i . '</span></a></li>';
            }
            if ($limitstart + 4 < $totalPage) $tmp_string .= '<li><span class="dots">...</span><li>';
            if ($limitstart + 3 < $totalPage) $tmp_string .= '<li><a href="' . $path . $str_limitstart . $totalPage . '"><span>' . $totalPage . '</span></a></li>';

            $string .= $tmp_string;
            if ($limitstart < $totalPage) {
                $string .= '<li><a href="' . $path . $str_limitstart . ($limitstart + 1) . '"><span>Trang sau</span></a></li>';
            }
            $string .= '</ul>';
        }
        return $string;
    }
}

?>