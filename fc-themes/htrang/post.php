<?php require_once($fc_themes . '/header.php'); ?>
<?php require_once($fc_themes . '/sidebar.php'); ?>
    <div class="content">
        <?php if ($fc_have_post) : ?>
            <?php foreach ($fc_posts as $post) { ?>
                <div class="post">
                    <h3><?php echo $post->title ?></h3>
                    <p class="infor_post">
                        <span><?php echo $post->date; ?></span>
                        in <a href="<?php echo $post->cat_link; ?>"><span><?php echo $post->cat_name; ?></span></a>
                        by <a href="<?php echo $post->author_link; ?>"><span><?php echo $post->author; ?></span></a></p>
                    <?php echo " " . $post->introl . $post->fulltext; ?>
                </div><!--End #post-->
            <?php } ?>
        <?php endif; ?>
    </div><!--End #content-->
<?php require_once($fc_themes . '/footer.php'); ?>