<?php
require_once('loader.php');
?>
<?php
if ($FcSession->is_logged_in() == false) {
    Helper::redirect_to('login.php');
}
require_once('includes/models/tbl_comment.php');
require_once('includes/models/tbl_cat.php');
$db = &$DBO;
$cat = new Tbl_Cat($db);
$rows_cat = $cat->getCat();
$where = array();
if (isset($_POST['cid'])) {
    if (count($_POST['cid']) > 0) {
        $cid = $_POST['cid'];
        require_once('includes/models/tbl_post.php');
        $Ob = new Tbl_Post($db);
        $array_error = array();
        foreach ($cid as $id) {
            if (Helper::isNumeric($id)) {
                $yes = $Ob->delete($id);
                //Delete comment liên quan đến bài viết đó.
                if ($yes) {
                    $sql = "DELETE FROM `tbl_comment` WHERE `id_post` = " . $id;
                    $db->setQuery($sql);
                    $db->query();
                }
            } else {
                $array_error[] = $id;
            }
        }

    }
}
if (isset($_POST['search']) && !empty($_POST['search'])) {
    $where[] = "a.`title` LIKE '%" . $db->escape_value($_POST['search']) . "%'";
}

if (isset($_POST['search_cat']) && $_POST['search_cat'] != '0' && !empty($_POST['search_cat'])) {
    $where[] = 'a.`id_cat` = ' . $db->escape_value($_POST['search_cat']) . '';
}

if (isset($_POST['search_publish']) && $_POST['search_publish'] != '') {
    $where[] = "a.`publish` = '" . $db->escape_value($_POST['search_publish']) . "'";
}

if (isset($_GET['beauty']) && $_GET['beauty'] != '' && Helper::isNumeric($_GET['beauty'])) {
    $where[] = "a.`beauty` = '" . $db->escape_value($_GET['beauty']) . "'";
}

if (!empty($where)) $query_where = ' WHERE ' . implode(' AND ', $where);


$query = "SELECT a.*, b.`user_name`, b.`display_name`, c.`cat_name` FROM `tbl_post` AS a "
    . "LEFT JOIN `tbl_user` as b ON a.`id_user` = b.`id` "
    . "LEFT JOIN `tbl_cat` as c ON a.`id_cat` = c.`id`" . $query_where . " ORDER BY a.`id` DESC";
$db->setQuery($query);
$rows = $db->loadObjectLists();
?>
<?php $sitename = "Quản lý bài viết | " . SITENAME ?>
<?php include('header.php') ?>
<div class="content">
    <!--Chú ý 1-->
    <form action="<?php echo FCPATH . 'fc-admin/edit.php'; ?>" name="formedit" method="post">
        <fieldset style="padding: 0px;">
            <table style="width: 100%; padding: 0px;">
                <tr>
                    <td><h2 id="edit-php" class="phplogo">Quản lý bài viết</h2></td>
                    <td align="right">
                        <div class="tool-bar">
                            <table style="padding: 0px;" align="right">
                                <tr align="center">
                                    <td><a href="<?php echo FCPATH; ?>fc-admin/post-new.php"><input type="button"
                                                                                                    value=""
                                                                                                    name="add-article"
                                                                                                    class="add-article"/></a>
                                    </td>
                                    <td><input type="button" value="" name="remove-trash" class="remove-trash"
                                               onclick="document.formedit.submit();"/></td>
                                </tr>
                                <tr align="center" style="font-size: 12px;">
                                    <td>Thêm bài viết</td>
                                    <td>Xóa bài viết</td>
                                </tr>
                            </table>
                        </div>
                    </td>
                </tr>
            </table>
        </fieldset>
        <fieldset style="padding: 0px;">
            <table style="width: 100%; padding: 0px;">
                <tr>
                    <td width="400">
                        <label style="padding-left: 10px;">Tìm tên bài: </label>
                        <input type="text" name="search" class="input-search"
                               value="<?php if (isset($_POST['search']) && !empty($_POST['search'])) {
                                   echo $_POST['search'];
                               } ?>"/>
                        <input type="button" class="search" name="search-button" value="Tìm"
                               onclick="document.formedit.submit();"/>
                        <input type="button" class="search" name="search-button" value="Xóa từ khóa"
                               onclick="document.formedit.search.value='';document.formedit.submit();"/>
                    </td>
                    <td align="right" style="padding-right: 10px;">
                        <select name="search_cat" style="font-size: 12; padding: 2px;">
                            <?php
                            $i = 0;
                            if ($_POST['search_cat'] == '0' || !isset($_POST['search_cat'])) {
                                echo '<option selected="selected" onclick="document.formedit.submit();" value="0">--Chọn chuyện mục--</option>';
                            } else {
                                echo '<option onclick="document.formedit.submit();" value="0">--Chọn chuyện mục--</option>';
                            }
                            foreach ($rows_cat as $row_cat) {
                                if ($row_cat->id == $_POST['search_cat']) {
                                    echo '<option selected="selected" onclick="document.formedit.submit();" value="' . $row_cat->id . '"> ' . $row_cat->cat_name . ' </option>';
                                    $i = 1;
                                } else {
                                    echo '<option onclick="document.formedit.submit();" value="' . $row_cat->id . '"> ' . $row_cat->cat_name . ' </option>';
                                }
                            }
                            ?>
                        </select>
                        <select name="search_publish" style="font-size: 12px; padding: 2px;">
                            <option <?php if (!isset($_POST['search_publish']) || $_POST['search_publish'] == '') echo 'selected="selected"'; ?>
                                    value="" onclick="document.formedit.submit();">--Trạng thái--
                            </option>
                            <option <?php if (($_POST['search_publish']) == '1') echo 'selected="selected"'; ?>
                                    value="1" onclick="document.formedit.submit();">--Công khai--
                            </option>
                            <option <?php if (($_POST['search_publish']) == '0') echo 'selected="selected"'; ?>
                                    value="0" onclick="document.formedit.submit();">--Bản nháp--
                            </option>
                        </select>
                    </td>
                </tr>
            </table>
        </fieldset>
        <!--Hết chú ý 1-->
        <?php if (!empty($array_error) || isset($FcSession->message)) { ?>
            <fieldset>
                <table>
                    <tr>
                        <td colspan="2">
                            <ul>
                                <?php
                                foreach ($array_error as $error) {
                                    echo "<li>Xóa bài viết thứ <span style='color:blue;'>" . $error . "</span> bị lỗi.</li>";
                                }
                                ?>
                            </ul>
                        </td>

                    </tr>
                </table>
            </fieldset>
        <?php } ?>
        <fieldset>
            <table style="width: 100%;" class="edit-php">
                <thead>
                <tr>
                    <th width="10">STT</th>
                    <th width="20">#</th>
                    <th width="332"> Tên bài viết</th>
                    <th width="75"> Trạng thái</th>
                    <th width="100">Hiển thị trang chủ</th>
                    <th width="120">Tác giả</th>
                    <th width="90">Chuyên mục</th>
                    <th width="140">Ngày tháng</th>
                    <th width="50">ID</th>
                </tr>
                </thead>

                <?php $i = 0;
                foreach ($rows as $row) {
                    $i++; ?>
                    <tr>
                        <td><?php echo $i; ?></td>
                        <td align="center"><input type="checkbox" name="cid[]" value="<?php echo $row->id; ?>"/></td>
                        <td align="left"><?php echo '<a href="' . FCPATH . 'fc-admin/post.php?post=' . $row->id . '&amp;action=edit">' . $row->title . '</a>'; ?></td>
                        <td><?php echo $row->publish ? 'Công khai' : 'Bản nháp'; ?></td>
                        <td><?php echo $row->beauty ? 'Có' : 'Không'; ?></td>
                        <td><?php if ($row->display_name == '') echo $row->user_name; else echo $row->display_name; ?></td>
                        <td><?php echo $row->cat_name; ?></td>
                        <td><?php echo $row->created; ?></td>
                        <td><?php echo $row->id; ?></td>
                    </tr>
                <?php } ?>

            </table>
        </fieldset>
    </form>
</div>
<?php include('footer.php') ?>    