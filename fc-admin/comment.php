<?php
require_once('loader.php');
?>
<?php
if ($FcSession->is_logged_in() == false) {
    Helper::redirect_to('login.php');
}
require_once('includes/models/tbl_comment.php');
$db = &$DBO;

if (isset($_GET['action']) && isset($_GET['cmid'])) {
    if ($_GET['action'] == 'publish') $status = '1'; else $status = '0';
    $action = $_GET['action'];
    $cmid = $_GET['cmid'];
    if (Helper::isNumeric($cmid)) {
        $data['id'] = $cmid;
        $data['status'] = $status;
        $Ob = new Tbl_Comment($db);
        if ($Ob->store($data)) {
            if ($status == '1') $FcSession->message = '<li>Bình luận<span style="color:blue"> ' . $cmid . '</span> đã được duyệt.</li>';
            else $FcSession->message = '<li>Bình luận<span style="color:red;"> ' . $cmid . '</span> đã đưa về trạng thái chờ duyệt.</li>';
        } else {
            Helper::redirect_to('index.php');
        }
    } else Helper::redirect_to('index.php');
}

$where = array();
if (isset($_POST['cid'])) {
    if (count($_POST['cid']) > 0) {
        $cid = $_POST['cid'];
        $Ob = new Tbl_Comment($db);
        $array_error = array();
        foreach ($cid as $id) {
            if (Helper::isNumeric($id)) {
                $Ob->delete($id);
            } else {
                $array_error[] = $id;
            }
        }
    }
}

if (Helper::isNumeric($_GET['status']) >= 0 && isset($_GET['status'])) {
    $where[] = "a.`status` = '" . $db->escape_value($_GET['status']) . "' ";
}

if (!empty($where)) $query_where = ' WHERE ' . implode(' AND ', $where);

$query = "SELECT a.*,b.title FROM `tbl_comment` AS a "
    . "LEFT JOIN `tbl_post` AS b ON a.`id_post` = b.`id`" . $query_where . "ORDER BY a.`status`,a.`id` DESC";
$db->setQuery($query);
$rows = $db->loadObjectLists();
?>
<?php $sitename = "Quản lý Bình luận | " . SITENAME ?>
<?php include('header.php') ?>
<div class="content">
    <form action="<?php echo FCPATH . 'fc-admin/comment.php'; ?>" name="formedit" method="post">
        <fieldset style="padding: 0px;">
            <table style="width: 100%; padding: 0px;">
                <tr>
                    <td><h2 id="comment-php" class="phplogo">Quản lý bình luận</h2></td>
                    <td align="right">
                        <div class="tool-bar">
                            <table style="padding: 0px;" align="right">
                                <tr align="center">
                                    <td><input type="button" value="" name="remove-trash" class="remove-trash"
                                               onclick="document.formedit.submit();"/></td>
                                </tr>
                                <tr align="center" style="font-size: 12px;">
                                    <td>Xóa bình luận</td>
                                </tr>
                            </table>
                        </div>
                    </td>
                </tr>
            </table>
        </fieldset>
        <fieldset style="padding: 0px;">
            <table style="width: 100%; padding: 0px;">
                <tr>
                    <td width="400">&nbsp;
                    </td>
                    <td align="right" style="padding-right: 10px;">
                        <a href="<?php echo FCPATH; ?>fc-admin/comment.php"><input class="search" type="button"
                                                                                   value="Tất cả bình luận"/></a>
                        <a href="<?php echo FCPATH; ?>fc-admin/comment.php?status=0"><input class="search" type="button"
                                                                                            value="Bình luận chờ duyệt"/></a>
                    </td>
                </tr>
            </table>
        </fieldset>
        <!--Hết chú ý 1-->
        <?php if (!empty($array_error) || isset($FcSession->message)) { ?>
            <fieldset>
                <table>
                    <tr>
                        <td colspan="2">
                            <ul>
                                <?php
                                if (isset($FcSession->message)) {
                                    echo $FcSession->message;
                                }
                                ?>
                                <?php
                                if (!empty($array_error)) {
                                    foreach ($array_error as $error) {
                                        echo "<li>Xóa bình luận thứ <span style='color:blue;'>" . $error . "</span> bị lỗi.</li>";
                                    }
                                }
                                ?>
                            </ul>
                        </td>
                    </tr>
                </table>
            </fieldset>
        <?php } ?>
        <fieldset>
            <table style="width: 100%;" class="edit-php">
                <thead>
                <tr>
                    <th width="10">STT</th>
                    <th width="20">#</th>
                    <th width="170"> Tên người bình luận</th>
                    <th width="300">Nội dung bình luận</th>
                    <th width="60">Trạng thái</th>
                    <th width="300">Bài viết liên quan</th>
                    <th width="30">ID</th>
                </tr>
                </thead>

                <?php $i = 0;
                foreach ($rows as $row) {
                    $i++; ?>
                    <tr>
                        <td><?php echo $i; ?></td>
                        <td align="center"><input type="checkbox" name="cid[]" value="<?php echo $row->id; ?>"/></td>
                        <td align="left" style="padding-left:4px;"><?php echo $row->com_name; ?></td>
                        <td align="left" style="padding-left:4px;">
                            <?php
                            echo '<em><span class="comment-date"> Ngày bình luận:</span> ' . $row->date_comment . "</em><br />";
                            echo '<p>' . $row->com_content . '</p>';
                            ?>
                        </td>
                        <td align="center">
                            <?php
                            echo $row->status ? '<a href="' . FCPATH . 'fc-admin/comment.php?action=unpublish&amp;cmid=' . $row->id . '" title="Chờ duyệt" class="comment-publish">Publish</a>'
                                : '<a href="' . FCPATH . 'fc-admin/comment.php?action=publish&amp;cmid=' . $row->id . '"  title="Duyệt bình luận" class="comment-unpublish">Unpublish</a>';
                            ?>
                        </td>
                        <td align="left" style="padding-left:4px;"><?php echo $row->title; ?></td>
                        <td><?php echo $row->id; ?></td>
                    </tr>
                <?php } ?>

            </table>
        </fieldset>
    </form>
</div>
<?php include('footer.php') ?>    