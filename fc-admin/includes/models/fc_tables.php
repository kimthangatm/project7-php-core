<?php

class FcTable
{
    var $_tbl_name = null;
    var $_tbl_key = null;
    var $_db = null;

    //Hàm khởi tạo
    function __construct($table, $key, &$db)
    {
        $this->_tbl_name = $table;
        $this->_tbl_key = $key;
        $this->_db = &$db;
    }

    /**
     * Hàm lấy các biến var trong đối tượng, Quay trở lại đối tượng Tbl_User ta có mấy biến var như sau
     * var $id           = null; 0
     * var $user_name    = null; 1
     * var $user_pass    = null; 2
     * var $fullname     = null; 3
     * var $user_sex     = null; 4
     * var $date         = null; 5
     * var $display_name = null; 6
     *
     * Vậy hàm này sẽ trả về 1 mảng tên của các biến var này:
     * với đối tượng Tbl_User thì ta có mảng
     * Array ( [0] => id [1] => user_name [2] => user_pass [3] => fullname [4] => user_sex [5] => date [6] => display_name
     *         [7] => _tbl_name [8] => _tbl_key [9] => _db )
     * Chú ý các phần tử từ 0->6 chính là tên các biến var trong class Tbl_User
     * và Các bạn để ý có phần tử 7 8 9...nó tương ứng với 3 biến của đối tượng class FcTable được khai báo ở dòng
     * 3 4 5 trong file này. (Tự tìm dòng 3 4 5 trong file này nhé)
     * À chú ý: Thống nhất khóa chính của bảng nằm trên cùng nhé. Kiểu như là Viết theo đúng thứ tự trường trong bảng.
     */
    function __fetchArrayField()
    {
        $class_vars = get_class_vars(get_class($this));
        $array_v = array();
        foreach ($class_vars as $var => $value) {
            /**
             * $value là cái quái gì mà nó chỉ xuất hiện 1 lần và ko có giá trị
             * Thực ra là $var=>$value sẽ trả về tên của biến đang xét nhưng nó ở dạng mảng hay đối tương mới đc
             * (Mới bít có thế, sai hay ko chịu :D)
             * Xem ví dụ test2.php, hỏi sâu nữa cũng chịu, xem ví dụ là hiểu
             */
            $array_v[] = $var;
        }
        return $array_v;
    }

    function store($data)
    {
        if ($data[$this->_tbl_key] == NULL) {
            return $this->__insert($data);
        } else {
            return $this->__update($data);
        }
    }

    function __insert(&$data)
    {
        $field_table = $this->__fetchArrayField();
        $sql = "INSERT INTO `" . $this->_tbl_name . "` SET ";
        for ($i = 1; $i < (count($field_table) - 3); $i++) {
            //if($data[$field_table[$i]]!=null){             
            if ($i < (count($field_table) - 4)) {
                $sql .= "`" . $field_table[$i] . "` = '" . $data[$field_table[$i]] . "', ";
            } else {
                $sql .= "`" . $field_table[$i] . "` = '" . $data[$field_table[$i]] . "'";
            }
            //}            

        }
        $sql = trim($sql);
        $this->_db->setQuery($sql);
        if ($resule = $this->_db->query()) {
            return true;
        } else {
            return false;
        }
    }

    function __update(&$data)
    {
        $field_table = $this->__fetchArrayField();
        $sql = "UPDATE `" . $this->_tbl_name . "` SET ";
        $set = array();
        for ($i = 1; $i < count($field_table) - 3; $i++) {
            if ($data[$field_table[$i]] != '') {
                $set[] .= "`" . $field_table[$i] . "` = '" . $data[$field_table[$i]] . "' ";
            }
        }
        $set_field = implode(',', $set);
        $sql .= $set_field;
        $sql .= " WHERE `" . $this->_tbl_key . "` = " . $data[$this->_tbl_key];
        $this->_db->setQuery($sql);
        if ($resule = $this->_db->query()) {
            return true;
        } else {
            return false;
        }
    }

    function __update_backup($data)
    {
        $field_table = $this->__fetchArrayField();
        $sql = "UPDATE `" . $this->_tbl_name . "` SET ";
        for ($i = 1; $i < count($field_table) - 3; $i++) {
            if ($data[$field_table[$i]] != '') {
                if ($i < (count($field_table) - 4)) {
                    $sql .= "`" . $field_table[$i] . "` = '" . $data[$field_table[$i]] . "', ";
                } else {
                    $sql .= "`" . $field_table[$i] . "` = '" . $data[$field_table[$i]] . "'";
                }
            }

        }

        $sql .= " WHERE `" . $this->_tbl_key . "` = " . $data[$this->_tbl_key];
        $this->_db->setQuery($sql);
        if ($resule = $this->_db->query()) {
            return true;
        } else {
            return false;
        }
    }


    //Xóa 1 bản ghi trong csdl
    //Vì chọn bản ghi thì ta dùng <input type="checkbox"..... /> nên đặt là $cid    
    function delete($cid = null)
    {
        $k = $this->_tbl_key;
        if ($cid) {
            $this->$k = (int)$cid;
        }

        $sql = "DELETE FROM `" . $this->_tbl_name . "` "
            . "\n WHERE `" . $this->_tbl_key . "`"
            . " = "
            . (is_int($this->$k) ? (int)$this->$k : "'" . $this->$k . ".");
        $this->_db->setQuery($sql);
        if ($result = $this->_db->query()) {
            return true;
        } else {
            return false;
        }
    }

    function &getNow()
    {
        $query = "SELECT NOW() AS 'fctime'";
        $this->_db->setQuery($query);
        $row = $this->_db->loadObject();
        return $row->fctime;
    }

    function &getRowsTable($fix = '')
    {
        $query = "SELECT count(`" . $this->_tbl_key . "`) AS 'rows_table' FROM `" . $this->_tbl_name . "` " . $fix;
        $this->_db->setQuery($query);
        $row = $this->_db->loadObject();
        return $row->rows_table;
    }

}

?>