<?php require_once('loader.php'); ?>
<?php $sitename = "Bảng điều khiển | " . SITENAME ?>
<?php include('header.php') ?>
<?php
if ($FcSession->is_logged_in() == false) {
    Helper::redirect_to('login.php');
}
?>
<?php
$db = &$DBO;

$query = "SELECT * FROM `tbl_user` WHERE `id`=" . $_SESSION['id'];
$db->setQuery($query);
$row = $db->loadObject();

$query1 = "SELECT count(`id`) as 'nums' FROM `tbl_post`";
$db->setQuery($query1);
$row1 = $db->loadObject();

$query2 = "SELECT count(`id`) as 'nums' FROM `tbl_comment` WHERE `status`=1";
$db->setQuery($query2);
$row2 = $db->loadObject();

$query3 = "SELECT count(`id`) as 'nums' FROM `tbl_cat`";
$db->setQuery($query3);
$row3 = $db->loadObject();

$query4 = "SELECT count(`id`) as 'nums' FROM `tbl_comment` WHERE `status`=0";
$db->setQuery($query4);
$row4 = $db->loadObject();

$query5 = "SELECT count(`id`) as 'nums' FROM `tbl_user`";
$db->setQuery($query5);
$row5 = $db->loadObject();
?>
<div class="content">
    <div class="main-content">
        <fieldset id="index-php">
            <table class="function-index">
                <tr>
                    <td><a href="<?php echo FCPATH; ?>fc-admin/post-new.php"><img src="images/icon-48-article-add.png"
                                                                                  alt="Thêm mới bài viết"
                                                                                  title="Thêm mới bài viết"/><br/>Thêm
                            mới bài viết</a></td>
                    <td><a href="<?php echo FCPATH; ?>fc-admin/edit.php"><img src="images/icon-48-article.png"
                                                                              alt="Quản lý bài viết"
                                                                              title="Quản lý bài viết"/><br/>Quản lý bài
                            viết</a></td>
                    <td><a href="<?php echo FCPATH; ?>fc-admin/edit.php?beauty=1"><img
                                    src="images/icon-48-frontpage.png" alt="Quản lý trang chủ"
                                    title="Quản lý trang chủ"/><br/>Quản lý trang chủ</a></td>
                    <td><a href="<?php echo FCPATH; ?>fc-admin/categories.php"><img src="images/icon-48-category.png"
                                                                                    alt="Quản lý chuyên mục"
                                                                                    title="Quản lý chuyên mục"/><br/>Quản
                            lý chuyên mục</a></td>
                </tr>
                <tr>
                    <td><a href="<?php echo FCPATH; ?>fc-admin/comment.php"><img src="images/comment.png"
                                                                                 alt="Quản lý bình luận"
                                                                                 title="Quản lý bình luận"/><br/>Quản lý
                            bình luận</a></td>
                    <td><a href="<?php echo FCPATH; ?>fc-admin/menu.php"><img src="images/icon-48-menumgr.png"
                                                                              alt=""/><br/>Quản lý menu</a></td>
                    <td><a href="<?php echo FCPATH; ?>fc-admin/profile.php"><img src="images/icon-48-user.png"
                                                                                 alt="Thông tin cá nhân"
                                                                                 title="Thông tin cá nhân"/><br/>Thông
                            tin cá nhân</a></td>
                    <td><a href="#"><img src="images/icon-48-config.png" alt="Cấu hình trang web"
                                         title="Cấu hình trang web"/><br/>Cấu hình trang web</a></td>
                </tr>
            </table>

            <table class="infomation-index">
                <tr>
                    <td colspan="2" align="center" style="text-align: center;">
                        Xin chào : <a href="<?php echo FCPATH . '/fc-admin/profile.php'; ?>"
                                      style="color: blue;"><?php echo $row->display_name != '' ? $row->display_name : $row->user_name; ?></a>
                    </td>
                </tr>
                <tr>
                    <td>Tổng số bài viết :</td>
                    <td> <?php echo $row1->nums; ?> </td>
                </tr>
                <tr>
                    <td>Tổng số chuyên mục:</td>
                    <td> <?php echo $row3->nums; ?> </td>
                </tr>
                <tr>
                    <td>Tổng số bình luận đã duyệt:</td>
                    <td> <?php echo $row2->nums; ?> </td>
                </tr>
                <tr>
                    <td>Số bình luận đang chờ duyệt</td>
                    <td> <?php echo $row4->nums; ?> </td>
                </tr>
                <tr>
                    <td>Tổng số thành viên</td>
                    <td> <?php echo $row5->nums; ?> </td>
                </tr>
            </table>
            <table class="infomation-index" style="float: right; margin: 0; margin-right: 10px;">
                <tr>
                    <td><img src="images/travel.png" alt="Travel" title="Travel"/></td>
                </tr>
            </table>
        </fieldset>
    </div>
</div>
<?php include('footer.php') ?>    