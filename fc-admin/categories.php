<?php require_once('loader.php'); ?>
<?php $sitename = "Quản lý chuyên mục | " . SITENAME ?>
<?php include('header.php') ?>
<?php
if ($FcSession->is_logged_in() == false) {
    Helper::redirect_to('login.php');
}
$db = &$DBO;
require_once('includes/models/tbl_cat.php');
require_once('includes/models/tbl_post.php');
$ObjectPost = new Tbl_Post($db);
if (isset($_POST['cid'])) {
    if (count($_POST['cid']) > 0) {
        $cid = $_POST['cid'];
        $Ob = new Tbl_Cat($db);
        $array_error = array();
        $xoa_thanh_cong = 0;
        foreach ($cid as $id) {
            if (Helper::isNumeric($id)) {
                if ($ObjectPost->getPost('', '', $id, '') != null) {
                    $array_error[] = $id;
                } else {
                    if ($Ob->delete($id)) {
                        $xoa_thanh_cong++;
                    }

                }
            } else {
                $array_error[] = $id;
            }
        }
    }
}
if (isset($_POST['submit'])) {
    $data = Helper::trimData($_POST);
    $data['cat_name'] = $db->escape_value($data['cat_name']);
    $data['cat_description'] = $data['cat_description'];
    if ($data['cat_slug'] == '') {
        $data['cat_slug'] = Helper::slugVN($data['cat_name']);
    }
    if ($data['cat_name'] != '') {
        $ObjectCat = new Tbl_Cat($db);
        $ObjectCat->store($data);
        if ($data['id'] == null)
            $not_error_data = "Chuyên mục <span style='color:blue;'>" . $data['cat_name'] . "</span> đã được tạo.";
        else $not_error_data = "Chuyên mục <span style='color:blue;'>" . $data['cat_name'] . "</span> đã được thay đổi.";
        $error_data = null;
    } else {
        $error_data = "Tên chuyên mục";
    }
} else {
    $data_edit = $_GET;
    if ($data_edit['action'] == 'edit' && Helper::isNumeric($data_edit['cat_id'])) {
        $sql = "SELECT * FROM `tbl_cat` WHERE `id`=" . $data_edit['cat_id'];
        $db->setQuery($sql);
        $row_edit = $db->loadObject();
        $viewEdit = true;
    }
}

//Lấy dữ liệu từ tbl_cat  
require_once('includes/models/tbl_cat.php');
$ObjectCat = new Tbl_Cat($db);
$rows = $ObjectCat->getCat();

?>
    <div class="content">
        <fieldset style="padding: 0;">
            <table style="width: 100%;">
                <tr>
                    <td><h2 id="categories-php" class="phplogo">Quản lý chuyên mục</h2></td>
                    <td align="right">
                        <div class="tool-bar">
                            <table style="padding: 0px;" align="right">
                                <tr align="center">
                                    <td><input type="button" value="" name="remove-trash" class="remove-trash"
                                               onclick="document.detalscategories.submit();"/></td>
                                </tr>
                                <tr align="center" style="font-size: 12px;">
                                    <td>Xóa chuyên mục</td>
                                </tr>
                            </table>
                        </div>
                    </td>
                </tr>
            </table>
        </fieldset>
        <?php if (isset($error_data) || isset($not_error_data) || isset($array_error) || $xoa_thanh_cong > 0): ?>
            <fieldset>
                <table>
                    <tr>
                        <td colspan="2">
                            <ul>
                                <?php
                                echo $xoa_thanh_cong ? "<li>Xóa thành công chuyên mục ID = <span style='color:blue;'>" . $xoa_thanh_cong . " </span></li>" : '';
                                foreach ($array_error as $aerror) {
                                    echo "<li>Xóa chuyên mục số<span style='color:red'> " . $aerror . "</span> không thành công do dữ liệu vẫn còn. Hãy làm trống dữ liệu chuyên mục có ID = <span style='color:red'>" . $aerror . "</span></li>";
                                }
                                echo $error_data ? "<li>Trường <span style='color:blue;'>" . $error_data . "</span> để trống.</li>" : '';
                                echo $not_error_data ? "<li><h3>" . $not_error_data . "</h3></li>" : '';
                                ?>
                            </ul>
                        </td>

                    </tr>
                </table>
            </fieldset>
        <?php endif; ?>
        <?php if ($viewEdit) { ?>
            <form method="post" action="<?php echo FCPATH . 'fc-admin/categories.php' ?>" name="addnewcategories">
                <fieldset style="float: left; width: 45%;">
                    <table width="100%">
                        <tr>
                            <td width="100">Tên chuyên mục</td>
                            <td><input style="width: 95%;" type="text" name="cat_name" class="ftext"
                                       value="<?php echo $row_edit->cat_name; ?>"/></td>
                        </tr>

                        <tr>
                            <td width="100">Tên viết tắt</td>
                            <td><input style="width: 95%;" type="text" name="cat_slug" class="ftext"
                                       value="<?php echo $row_edit->cat_slug; ?>"/></td>
                        </tr>
                        <tr>
                            <td width="100" valign="top">Mô tả</td>
                            <td><textarea style="width: 96%;" cols="10" rows="10"
                                          name="cat_description"><?php echo $row_edit->cat_description; ?></textarea>
                            </td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td align="left">
                                <input type="submit" class="search"
                                       style="padding:5px; margin-left: 0; margin-top: 10px;" value="Lưu chỉnh sửa"
                                       name="submit"/>
                                <input type="hidden" name="id" value="<?php echo $row_edit->id; ?>"/>
                            </td>
                        </tr>
                    </table>
                </fieldset>
            </form>
        <?php } else { ?>
            <form method="post" action="<?php echo FCPATH . 'fc-admin/categories.php' ?>" name="addnewcategories">
                <fieldset style="float: left; width: 45%;">
                    <table width="100%">
                        <tr>
                            <td width="100">Tên chuyên mục</td>
                            <td><input style="width: 95%;" type="text" name="cat_name" class="ftext" value=""/></td>
                        </tr>

                        <tr>
                            <td width="100">Tên viết tắt</td>
                            <td><input style="width: 95%;" type="text" name="cat_slug" class="ftext" value=""/></td>
                        </tr>
                        <tr>
                            <td width="100" valign="top">Mô tả</td>
                            <td><textarea style="width: 96%;" rows="10" cols="10" name="cat_description"></textarea>
                            </td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td align="left">
                                <input type="submit" class="search"
                                       style="padding:5px; margin-left: 0; margin-top: 10px;" value="Tạo chuyên mục"
                                       name="submit"/>
                                <input type="hidden" name="id" value=""/>
                            </td>
                        </tr>
                    </table>
                </fieldset>
            </form>
        <?php } ?>
        <form method="post" action="<?php echo FCPATH . 'fc-admin/categories.php' ?>" name="detalscategories">
            <fieldset style="float: right;">
                <table class="categories-php">
                    <thead>
                    <tr>
                        <th width="10">STT</th>
                        <th width="20">#</th>
                        <th width="170">Tên chuyên mục</th>
                        <th width="200">Mô tả</th>
                        <th width="40">ID</th>
                    </tr>
                    </thead>

                    <?php $i = 0;
                    foreach ($rows as $row) {
                        $i++; ?>
                        <tr>
                            <td><?php echo $i; ?></td>
                            <td><input type="checkbox" name="cid[]" value="<?php echo $row->id; ?>"/></td>
                            <td align="left"><a
                                        href="<?php echo FCPATH . 'fc-admin/categories.php?action=edit&amp;cat_id=' . $row->id; ?>"><?php echo $row->cat_name ?></a>
                            </td>
                            <td align="left"><?php echo $row->cat_description; ?></td>
                            <td><?php echo $row->id; ?></td>
                        </tr>
                    <?php } ?>
                </table>
            </fieldset>
        </form>
    </div>
    <div class="clear"></div>
<?php include('footer.php') ?>