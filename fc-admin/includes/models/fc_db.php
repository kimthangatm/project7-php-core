<?php

class f4Db
{

    private $_connection;
    public $_sql;    //last_query
    public $_error;

    function __construct()
    {
        $this->openConnection();
        $this->magic_quotes_active = get_magic_quotes_gpc();
        $this->real_escape_string_exists = function_exists("mysql_real_escape_string");
    }

    function &openConnection()
    {
        $this->_connection = mysql_connect(DB_HOST, DB_USER, DB_PASS);
        if (!$this->_connection) {
            die("Can not connection to DB_HOST");
        } else {
            mysql_set_charset('utf8');
            $db_select = mysql_select_db(DB_NAME, $this->_connection);
            if (!$db_select) {
                die("Can not selected DB_NAME");
            }
        }
    }

    function &closeConnection()
    {
        if ($this->_connection) {
            mysql_close($this->_connection);
            unset($this->_connection);
        }
    }

    function &setQuery($sql)
    {
        $this->_sql = $sql;
        return $sql;
    }

    function &loadObjectLists($key = '')
    {
        if (!($cur = $this->query())) {
            return null;
        }
        $array = array();
        while ($row = mysql_fetch_object($cur)) {
            if ($key) {
                $array[$row->$key] = $row;
            } else {
                $array[] = $row;
            }
        }
        mysql_free_result($cur);
        return $array;
    }

    function &loadObject($key = '')
    {
        if (!($cur = $this->query())) {
            return null;
        }
        $object = mysql_fetch_object($cur);
        mysql_free_result($cur);
        return $object;
    }

    function &query()
    {
        $result = mysql_query($this->_sql);
        if (!$result) {
            $this->_error = mysql_error();
        }
        return $result;
    }

    function &getListCount()
    {
        if ($rows = $this->query()) {
            return mysql_num_rows($rows);
        } else {
            return 0;
        }
    }

    function escape_value($value)
    {
        if ($this->real_escape_string_exists) { // PHP v4.3.0 or higher
            // undo any magic quote effects so mysql_real_escape_string can do the work
            if ($this->magic_quotes_active) {
                $value = stripslashes($value);
            }
            $value = mysql_real_escape_string($value);
        } else { // before PHP v4.3.0
            // if magic quotes aren't already on then add slashes manually
            if (!$this->magic_quotes_active) {
                $value = addslashes($value);
            }
            // if magic quotes are active, then the slashes already exist
        }
        return $value;
    }

}

$DBO = new f4Db();

?>