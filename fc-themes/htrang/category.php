<?php require_once($fc_themes . '/header.php'); ?>
<?php require_once($fc_themes . '/sidebar.php'); ?>
    <div class="content">
        <?php if ($fc_have_post) : ?>
            <?php foreach ($fc_posts as $post) { ?>
                <div class="post">
                    <img class="thumbnail" src="<?php echo $post->thumbnail_link ?>" width="120" height="120"
                         title="<?php echo $post->title; ?>" alt="<?php echo $post->title; ?>"/>
                    <h3><a href="<?php echo $post->post_link; ?>"><?php echo $post->title ?></a></h3>
                    <p class="infor_post">
                        <span><?php echo $post->date; ?></span>
                        in <a href="<?php echo $post->cat_link; ?>"><span><?php echo $post->cat_name; ?></span></a>
                        by <a href="<?php echo $post->author_link; ?>"><span><?php echo $post->author; ?></span></a></p>
                    <?php echo " " . $post->introl; ?>
                    <a class="link" href="<?php echo $post->post_link; ?>">Xem chi tiết hơn</a>
                </div><!--End #post-->
            <?php } ?>
            <?php echo $fc_paging; ?>
        <?php endif; ?>
    </div><!--End #content-->
<?php require_once($fc_themes . '/footer.php'); ?>