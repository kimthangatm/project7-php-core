<?php require_once('loader.php'); ?>
<?php $sitename = "Thêm bài viết | " . SITENAME ?>
<?php include('header.php') ?>
<?php
if ($FcSession->is_logged_in() == false) {
    Helper::redirect_to('login.php');
}
?>
<?php
require_once('includes/models/tbl_cat.php');
require_once('includes/models/tbl_post.php');
$db = &$DBO;
$cat = new Tbl_Cat($db);
$rows = $cat->getCat();
if (isset($_POST['submit'])) {
    $array1 = array('title', 'thumbnail', 'CKEditor1');
    $array2 = array('Tiêu đề bài viêt', 'Ảnh đại diện', 'Nội dung');
    $array_error = Helper::checkField($array1, $array2);
    //var_dump($_POST);echo "-OK-";exit();
    if (count($array_error) == 0) {
        $data = Helper::trimData($_POST);
        $data['title'] = $db->escape_value($data['title']);
        if ($data['slug_post'] == '') {
            $data['slug_post'] = Helper::slugVN($data['title']);
        }
        $data['thumbnail'] = $db->escape_value($data['thumbnail']);
        if ($data['beauty'] != '0' && $data['beauty'] != '1') {
            Helper::redirect_to(index . php);
        }
        if ($data['publish'] != '0' && $data['publish'] != '1') {
            Helper::redirect_to(index . php);
        }
        $num = strpos($data[CKEditor1], '&lt;!--more--&gt;');
        if ($num == false || $num == 0) {
            $data['introl'] = $db->escape_value($data['CKEditor1']);
            $data['fulltext'] = '';
        } else if ($num > 0) {
            $data['introl'] = $db->escape_value(substr($data['CKEditor1'], 0, $num));
            $data['fulltext'] = $db->escape_value(substr($data['CKEditor1'], $num + 17, strlen($data['CKEditor1']) - $num + 17));
        }
        $data['tag'] = $db->escape_value($data['tag']);
        $data['id'] = null;
        $data['status'] = $db->escape_value($data['status']);
        $data['id_user'] = $FcSession->id;
        if ($data['created'] == '') $data['created'] = $ObjectCat->getNow(); else $data['created'] = $db->escape_value($data['created']);
        $ObjectPost = new Tbl_Post($db);

        if ($insert_ok = $ObjectPost->store($data)) {
            Helper::redirect_to('edit.php');
        } else {
            $FcSession->message = "Lỗi đăng bài viết!";
        }

    } else {
        $FcSession->message = "Lỗi đăng bài viết!";
    }
}
?>
    <div class="content">
        <form method="post" action="<?php echo FCPATH . 'fc-admin/post-new.php' ?>" name="test3" class="fc-post-php">
            <fieldset style="padding: 0px;">
                <table style="width: 100%; padding: 0px;">
                    <tr>
                        <td><h2 id="post-php" class="phplogo">Thêm mới bài viết</h2></td>
                        <td align="right">
                            <div class="tool-bar">
                                <table style="padding: 0px;" align="right">
                                    <tr align="center">
                                        <td><input type="submit" name="submit" class="save-submit"
                                                   value="Lưu bài viết"/></td>
                                        <td><a href="<?php echo FCPATH . 'fc-admin/edit.php' ?>"><input type="button"
                                                                                                        name="cancel"
                                                                                                        class="cancel"
                                                                                                        value="Lưu bài viết"/></a>
                                        </td>
                                    </tr>
                                    <tr align="center">
                                        <td>Lưu bài viết</td>
                                        <td>Hủy</td>
                                    </tr>
                                </table>
                            </div>
                        </td>
                    </tr>
                </table>
            </fieldset>

            <?php if (!empty($array_error) || isset($FcSession->message)) { ?>
                <fieldset>
                    <table>
                        <tr>
                            <td colspan="2">
                                <ul>
                                    <?php
                                    foreach ($array_error as $error) {
                                        echo "<li>Trường <span style='color:blue;'>" . $error . "</span> để trống.</li>";
                                    }
                                    if (isset($FcSession->message)) {
                                        echo "<li>Thông báo:<span style='color:blue;'>" . $FcSession->message . "</span></li>";
                                    }
                                    ?>
                                </ul>
                            </td>

                        </tr>
                    </table>
                </fieldset>
            <?php } ?>

            <fieldset>
                <table>
                    <tr>
                        <td width="150"><label>Tên bài viết:</label></td>
                        <td><input type="text" name="title" class="ftext" value="<?php echo $_POST['title']; ?>"/></td>
                    </tr>
                    <tr>
                        <td><label>Bí danh:</label></td>
                        <td><input type="text" name="slug_post" class="ftext"
                                   value="<?php echo $_POST['slug_post']; ?>"/></td>
                    </tr>
                    <tr>
                        <td><label>Ảnh đại diện:</label></td>
                        <td><input type="text" name="thumbnail" class="ftext"
                                   value="<?php echo $_POST['thumbnail']; ?>"/></td>
                    </tr>
                    <tr>
                        <td><label>Hiển thị tai trang chủ:</label></td>
                        <td>
                            <input type="radio" name="beauty" value="1"/><label> Có </label>
                            <input type="radio" name="beauty" checked="checked" value="0"/><label> Không </label>
                        </td>
                    </tr>
                    <tr>
                        <td>Trạng thái</td>
                        <td>
                            <input type="radio" name="publish" checked="checked" value="1"/><label> Công khai </label>
                            <input type="radio" name="publish" value="0"/><label> Bản nháp </label>
                        </td>
                    </tr>
                    <tr>
                        <td>Chuyên mục</td>
                        <td>
                            <select name="id_cat">

                                <?php
                                $i = 0;
                                foreach ($rows as $row) {
                                    $i++;

                                    if ($i == 1) {
                                        echo '<option selected="selected" value="' . $row->id . '"> ' . $row->cat_name . ' </option>';
                                    } else {
                                        echo '<option value="' . $row->id . '"> ' . $row->cat_name . ' </option>';
                                    }

                                }
                                ?>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td valign="top"><label>Nội dung</label></td>
                        <td>
                            <?php
                            include_once('ckeditor/ckeditor.php');
                            include_once('ckfinder/ckfinder.php');
                            $ckeditor = new CKEditor();
                            $ckeditor->basePath = 'ckeditor/';
                            $ckeditor->config['filebrowserImageBrowseUrl'] = 'ckfinder/ckfinder.html?type=Images';
                            $ckeditor->config['filebrowserFlashBrowseUrl'] = 'ckfinder/ckfinder.html?type=Flash';
                            $ckeditor->config['filebrowserUploadUrl'] = 'ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files';
                            $ckeditor->config['filebrowserImageUploadUrl'] = 'ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images';
                            $ckeditor->config['filebrowserFlashUploadUrl'] = 'ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash';
                            $ckeditor->editor('CKEditor1');
                            ?>
                        </td>
                    </tr>
                    <tr>
                        <td>Từ khóa</td>
                        <td><input type="text" name="tag" class="ftext" value="<?php echo $_POST['tag']; ?>"/></td>
                    </tr>
                    <tr>
                        <td>Ngày khởi tạo</td>
                        <td><input type="text" name="created" class="ftext" value="<?php echo $cat->getNow(); ?>"
                                   style="width: 140px;"/><input type="hidden" name="id"
                                                                 value="<?php echo $row->id; ?>"/></td>
                    </tr>
                    <tr>
                        <td>Cho phép bình luận</td>
                        <td><input type="radio" checked="checked" name="status" value="1"/> Có <input type="radio"
                                                                                                      name="status"
                                                                                                      value="0"/> Không
                        </td>
                    </tr>
                </table>
            </fieldset>
            <input type="hidden" name="id" value=""/>
        </form>
    </div>
<?php include('footer.php') ?>