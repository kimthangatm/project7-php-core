<?php
    require_once('loader.php');
    $db = & $DBO;
    $array_id = $_GET;
    $limitstart = Helper::isNumeric($array_id['limitstart']);    
    $fc_sitename = SITENAME;
    $fc_path = FCPATH;
       
    $pageName = strtolower(basename( $_SERVER['PHP_SELF']));     
    if($pageName=='index.php') $fc_is_home = true; else $fc_is_home = false;
     
    $fc_themes = TEMP_DIRECTORY;
    if(($id_p=Helper::isNumeric($array_id['p']))>0){
        //Reset xem thử chắc ăn :D
        unset($array_id);        
        $ObFcPost = new Fc_Post($id_p,'p','',$limitstart,$db);
        //$fc_paging = $ObFcPost->getPaging();
        $fc_paging = null;
        $fc_posts  = $ObFcPost->getPost();                
        $fc_have_post = $ObFcPost->getHavePosts();
        if($fc_have_post){             
            $fc_title = $fc_posts[0]->title;           
        }else{
            $fc_title = '';
        }        
        require_once(TEMP_DIRECTORY.'/post.php');                        
    }else 
        if(($id_cat=Helper::isNumeric($array_id['cat']))>0){
            unset($array_id);
            $ObFcPost = new Fc_Post($id_cat,'cat','',$limitstart,$db);                        
            $fc_paging    = $ObFcPost->getPaging(); 
            $fc_posts     = $ObFcPost->getPost();
            $fc_have_post = $ObFcPost->getHavePosts();
            $query = "SELECT `cat_name` FROM `tbl_cat` WHERE `id`=".$id_cat." LIMIT 1";
            $db->setQuery($query);
            $row = $db->loadObject();
            $fc_title = $row->cat_name;                
            require_once(TEMP_DIRECTORY.'/category.php');           
        }else
            if(($id_page=Helper::isNumeric($array_id['page']))>0){
                unset($array_id);
                $ObFcPost = new Fc_Post($id_page,'page','',$limitstart,$db);
                //$fc_paging = $ObFcPost->getPaging();
                $fc_paging = null;
                $fc_posts  = $ObFcPost->getPost();
                $fc_have_post = $ObFcPost->getHavePosts();
                if($fc_have_post){             
                    $fc_title = $fc_posts[0]->title; 
                }else{
                    $fc_title = '';
                }  
                require_once(TEMP_DIRECTORY.'/page.php');
            }else
                if(($id_author=Helper::isNumeric($array_id['author']))>0){
                    unset($array_id);                   
                    $ObFcPost = new Fc_Post($id_author,'author','',$limitstart,$db);
                    $fc_paging = $ObFcPost->getPaging();                    
                    $fc_posts  = $ObFcPost->getPost();
                    $fc_have_post = $ObFcPost->getHavePosts(); 
                    if($fc_have_post){             
                        $fc_title = $fc_posts[0]->title; 
                    }else{
                        $fc_title = '';
                    }                                     
                    require_once(TEMP_DIRECTORY.'/author.php');
                }else{
                    unset($array_id);                                        
                    $ObFcPost = new Fc_Post('','','1',$limitstart,$db);
                    $fc_paging    = $ObFcPost->getPaging(); 
                    $fc_posts     = $ObFcPost->getPost();
                    $fc_have_post = $ObFcPost->getHavePosts();
                    $fc_title = null;   
                    require_once(TEMP_DIRECTORY.'/index.php');                
                }        	
?>
