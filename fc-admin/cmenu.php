<?php require_once('loader.php'); ?>
<?php $sitename = "Quản lý menu | " . SITENAME ?>
<?php include('header.php') ?>
<?php
if ($FcSession->is_logged_in() == false) {
    Helper::redirect_to('login.php');
}
$db = &$DBO;
require_once('includes/models/tbl_cmenu.php');
if (!($parent = Helper::isNumeric($_GET['Itemid']))) {
    Helper::redirect_to('index.php');
}
if (isset($_POST['cid'])) {
    if (count($_POST['cid']) > 0) {
        $cid = $_POST['cid'];
        $Ob = new Tbl_CMenu($db);
        $array_error = array();
        $xoa_thanh_cong = 0;
        foreach ($cid as $id) {
            if (Helper::isNumeric($id)) {
                if ($Ob->delete($id)) {
                    $xoa_thanh_cong++;
                }
            } else {
                $array_error[] = $id;
            }
        }
    }
}

if (isset($_POST['submit'])) {
    $data = Helper::trimData($_POST);
    $data['name'] = $db->escape_value($data['name']);
    $data['id_pmenu'] = $parent;
    if ($data['slug'] == '') {
        $data['slug'] = Helper::slugVN($data['name']);
    }
    if ($data['name'] != '' and $data['link'] != '') {
        $ObjectPmenu = new Tbl_CMenu($db);
        if ($ObjectPmenu->store($data)) {
            if ($data['id'] == null)
                $not_error_data = "Menu con <span style='color:blue;'>" . $data['name'] . "</span> đã được tạo.";
            else $not_error_data = "Menu con <span style='color:blue;'>" . $data['name'] . "</span> đã được thay đổi.";
            $error_data = null;
        } else {
            $not_error_data = "Lỗi tạo menu do <span style='color:red;'> Tên viết tắt bị trùng lặp</span>. Vui lòng xem lại!";
        }
    } else {
        $error_data = "Tên tiêu đề hoặc Đường link";
    }
} else {
    $data_edit = $_GET;
    if ($data_edit['action'] == 'edit' && Helper::isNumeric($data_edit['mid'])) {
        $sql = "SELECT * FROM `tbl_cmenu` WHERE `id`=" . $data_edit['mid'];
        $db->setQuery($sql);
        $row_edit = $db->loadObject();
        $viewEdit = true;
    }
}

//Lấy dữ liệu từ tbl_pmenu      
$query = "SELECT `id` FROM `tbl_pmenu` WHERE `id`=" . $parent;
$db->setQuery($query);
if ($db->getListCount() > 0) {
    $query = "SELECT * FROM `tbl_cmenu` WHERE `id_pmenu`=" . $parent . ' ORDER BY `id` ASC';
    $db->setQuery($query);
    $rows = $db->loadObjectLists();
} else {
    Helper::redirect_to('index.php');
}

?>
    <div class="content">
        <fieldset style="padding: 0;">
            <table style="width: 100%;">
                <tr>
                    <td><h2 id="menu-php" class="phplogo">Quản lý menu</h2></td>
                    <td align="right">
                        <div class="tool-bar">
                            <table style="padding: 0px;" align="right">
                                <tr align="center">
                                    <td><input type="button" value="" name="remove-trash" class="remove-trash"
                                               onclick="document.detalsitem.submit();"/></td>
                                </tr>
                                <tr align="center" style="font-size: 12px;">
                                    <td>Xóa item</td>
                                </tr>
                            </table>
                        </div>
                    </td>
                </tr>
            </table>
        </fieldset>
        <?php if (isset($error_data) || isset($not_error_data) || isset($array_error) || $xoa_thanh_cong > 0): ?>
            <fieldset>
                <table>
                    <tr>
                        <td colspan="2">
                            <ul>
                                <?php
                                if ($xoa_thanh_cong > 0)
                                    echo $xoa_thanh_cong ? "<li>Xóa thành công menu con ID = <span style='color:blue;'>" . $xoa_thanh_cong . " </span></li>" : '';
                                if (isset($array_error)) {
                                    foreach ($array_error as $aerror) {
                                        echo "<li>Xóa menu số<span style='color:red'> " . $aerror . "</span> không thành công</li>";
                                    }
                                }
                                echo $error_data ? "<li>Trường <span style='color:blue;'>" . $error_data . "</span> để trống. Nếu bạn muốn đường link trống xin gõ <span style='color:blue'> # </span></li>" : '';
                                if (isset($not_error_data))
                                    echo $not_error_data ? "<li><h3>" . $not_error_data . "</h3></li>" : '';
                                ?>
                            </ul>
                        </td>

                    </tr>
                </table>
            </fieldset>
        <?php endif; ?>
        <?php if ($viewEdit) { ?>
            <form method="post" action="<?php echo FCPATH . 'fc-admin/cmenu.php?Itemid=' . $parent; ?>" name="editmenu">
                <fieldset style="float: left; width: 45%;">
                    <table width="100%">
                        <tr>
                            <td width="100">Tiêu đề</td>
                            <td><input style="width: 95%;" type="text" name="name" class="ftext"
                                       value="<?php echo $row_edit->name; ?>"/></td>
                        </tr>

                        <tr>
                            <td width="100">Tên viết tắt</td>
                            <td><input style="width: 95%;" type="text" name="slug" class="ftext"
                                       value="<?php echo $row_edit->slug; ?>"/></td>
                        </tr>
                        <tr>
                            <td width="100" valign="top">Đường link</td>
                            <td><input type="text" name="link" value="<?php echo $row_edit->link; ?>" class="ftext"
                                       style="width: 95%;"/></td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td align="left">
                                <input type="submit" class="search"
                                       style="padding:5px; margin-left: 0; margin-top: 10px;" value="Lưu chỉnh sửa"
                                       name="submit"/>
                                <a href="<?php echo FCPATH . 'fc-admin/cmenu.php?Itemid=' . $parent; ?>"><input
                                            type="button" name="button1"
                                            style="padding:5px; margin-left: 0; margin-top: 10px;" value="Hủy chỉnh sửa"
                                            class="search"/></a>
                                <input type="hidden" name="id" value="<?php echo $row_edit->id; ?>"/>
                            </td>
                        </tr>
                    </table>
                </fieldset>
            </form>
        <?php } else { ?>
            <form method="post" action="<?php echo FCPATH . 'fc-admin/cmenu.php?Itemid=' . $parent; ?>"
                  name="addnewmenu">
                <fieldset style="float: left; width: 45%;">
                    <table width="100%">
                        <tr>
                            <td width="100">Tiêu đề</td>
                            <td><input style="width: 95%;" type="text" name="name" class="ftext" value=""/></td>
                        </tr>

                        <tr>
                            <td width="100">Tên viết tắt</td>
                            <td><input style="width: 95%;" type="text" name="slug" class="ftext" value=""/></td>
                        </tr>
                        <tr>
                            <td width="100" valign="top">Đường link</td>
                            <td><input type="text" name="link" value="" class="ftext" style="width: 95%;"/></td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td align="left">
                                <input type="submit" class="search"
                                       style="padding:5px; margin-left: 0; margin-top: 10px;" value="Tạo link"
                                       name="submit"/>
                                <a href="<?php echo FCPATH . 'fc-admin/cmenu.php?Itemid=' . $parent; ?>"><input
                                            type="button" name="button1"
                                            style="padding:5px; margin-left: 0; margin-top: 10px;" value="Hủy"
                                            class="search"/></a>
                                <input type="hidden" name="id" value=""/>
                            </td>
                        </tr>
                    </table>
                </fieldset>
            </form>
        <?php } ?>
        <form method="post" action="<?php echo FCPATH . 'fc-admin/cmenu.php?Itemid=' . $parent; ?>" name="detalsitem">
            <fieldset style="float: right;">
                <table class="categories-php">
                    <thead>
                    <tr>
                        <th width="10">STT</th>
                        <th width="20">#</th>
                        <th width="160">Tên menu</th>
                        <th width="160">Tên viết tắt <span style="color: red;">(*)</span></th>
                        <th width="40">ID</th>
                    </tr>
                    </thead>

                    <?php $i = 0;
                    foreach ($rows as $row) {
                        $i++; ?>
                        <tr>
                            <td><?php echo $i; ?></td>
                            <td><input type="checkbox" name="cid[]" value="<?php echo $row->id; ?>"/></td>
                            <td align="left" style="padding-left: 5px;"><a
                                        href="<?php echo FCPATH . 'fc-admin/cmenu.php?action=edit&amp;mid=' . $row->id . '&Itemid=' . $parent; ?>"><?php echo $row->name ?></a>
                            </td>
                            <td align="left" style="padding-left: 5px;"><?php echo $row->slug; ?></td>
                            <td><?php echo $row->id; ?></td>
                        </tr>
                    <?php } ?>
                    <tfoot>
                    <tr>
                        <td colspan="6"><span style="color: red;">(*)</span> Tên viết tắt không trùng nhau</td>
                    </tr>
                    </tfoot>
                </table>
            </fieldset>
        </form>
    </div>
    <div class="clear"></div>
<?php include('footer.php') ?>