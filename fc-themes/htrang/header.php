<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8"/>
    <meta name="author" content="ZaiChi"/>
    <link rel="shortcut icon" href="<?php echo $fc_path . $fc_themes ?>/images/favicon.ico" type="image/x-icon"/>
    <link href="<?php echo $fc_path . $fc_themes ?>/css/style.css" rel="stylesheet" type="text/css"/>
    <title><?php if ($fc_is_home) echo $fc_sitename; else echo $fc_title . ' | ' . $fc_sitename; ?></title>
    <script type="text/javascript" src="<?php echo $fc_path . $fc_themes ?>/js/jquery.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $('.banner img').addClass('caption');
            $('.banner img:gt(0)').hide();
            setInterval(function () {
                    $('.banner :first-child').fadeOut()
                        .next('img').fadeIn()
                        .end().appendTo('.banner');
                },
                4000);
        });
    </script>
</head>
<body style="padding-bottom:30px;">
<div id="wrapper">
    <div class="header">
        <div class="top">
            <h2><a class="title" href="<?php echo FCPATH ?>"><span class="s1">Landscape</span><span
                            class="s2">Travel</span></a></h2>
            <ul>
                <?php
                $db =& $DBO;
                $query = "select a.* from `tbl_cmenu` as a left join `tbl_pmenu` as b on a.`id_pmenu` = b.`id`
                where b.`slug`='top-menu' order by `id` asc";
                $db->setQuery($query);
                $rows_menu = $db->loadObjectLists();
                foreach ($rows_menu as $row_menu) {
                    echo "<li><a href=" . $row_menu->link . ">" . $row_menu->name . "</a></li>";
                }
                ?>
            </ul>

        </div><!--End #top-->
        <div class="banner">
            <img class="caption" src="<?php echo $fc_path . $fc_themes ?>/images/pic1.jpg" width="750" height="198"/>
            <img class="caption" src="<?php echo $fc_path . $fc_themes ?>/images/pic3.jpg" width="750" height="198"/>
            <img class="caption" src="<?php echo $fc_path . $fc_themes ?>/images/pic4.jpg" width="750" height="198"/>
        </div><!--End #banner-->
    </div><!--End #header-->