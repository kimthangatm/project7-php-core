<?php

class Tbl_Post extends FcTable
{

    var $id = null;
    var $title = null;
    var $slug_post = null;
    var $thumbnail = null;
    var $introl = null;
    var $fulltext = null;
    var $id_user = null;
    var $id_cat = null;
    var $publish = null;
    var $tag = null;
    var $created = null;
    var $beauty = null;
    var $status = null;

    function __construct(&$db)
    {
        parent::__construct('tbl_post', 'id', $db);
    }

    // ham dem so bai viet 
    function countPost()
    {
        $query = "SELECT count(`id`) as 'cpost' FROM `tbl_post`";
        $this->_db->setQuery($query);
        $row = $this->_db->loadObject();
        return $row->cpost;
    }

    // ham lay noi dung bai viet theo id 
    function getPost($id = '', $beauty = '', $id_cat = '', $publish = '')
    {
        $tg = array();
        $query = "SELECT * FROM `tbl_post`";

        if ($id) {
            $tg[] = "`id`= " . $id;
        }

        if ($beauty) {
            $tg[] = "`beauty`= '" . $beauty . "'";
        }

        if ($id_cat) {
            $tg[] = "`id_cat`= " . $id_cat;
        }

        if ($publish) {
            $tg[] = "`publish`= '" . $publish . "'";
        }

        $query1 = implode(" AND ", $tg);
        if (count($tg) > 0) {
            $query .= " WHERE ";
        }
        $query .= $query1;
        $this->_db->setQuery($query);
        return $this->_db->loadObjectLists();

    }
}

?>