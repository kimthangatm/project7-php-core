<?php

class Session
{
    private $logged_in = false;
    public $id;//id của User
    public $message;

    function __construct()
    {
        session_start();
        $this->check_login();
        $this->check_message();
        if ($this->logged_in) {
            //Lam viec
        } else {
            //Khong lam viec
        }
    }

    public function is_logged_in()
    {
        return $this->logged_in;
    }

    private function check_login()
    {
        if (isset($_SESSION['id'])) {
            $this->logged_in = true;
            $this->id = $_SESSION['id'];
        } else {
            unset($this->id);
            $this->logged_in = false;
        }
    }

    private function check_message()
    {
        if (isset($_SESSION['message'])) {
            $this->message = $_SESSION['message'];
            unset($_SESSION['message']);
        } else {
            $this->message = null;
        }
    }

    public function login($user)
    {
        if ($user) {
            $this->logged_in = true;
            $this->id = $user->id;
            $_SESSION['id'] = $user->id;
        }
    }

    public function logout()
    {
        unset($this->id);
        unset($_SESSION['id']);
        $this->logged_in = false;
    }

}

$FcSession = new Session();
?>