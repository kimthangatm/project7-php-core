<?php
$db = &$DBO;
$query = "SELECT * FROM `tbl_pmenu` ORDER BY `id` ASC";
$db->setQuery($query);
$p_menu = $db->loadObjectLists();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8"/>
    <meta name="author" content="ohyeah"/>
    <link href="css/default1.css" rel="stylesheet" type="text/css"/>
    <link rel="shortcut icon" href="http://it-tuts.com/wp-content/themes/ittuts/images/ittut.ico" type="image/x-icon"/>
    <title><?php echo $sitename; ?></title>
</head>
<body>
<div class="wrap">
    <div class="header">
        <div class="logo">
        </div>
        <div>
            <ul class="menu-main">
                <?php if ($FcSession->is_logged_in() == true) { ?>
                    <li class="parent"><a href="<?php echo FCPATH . 'fc-admin/index.php'; ?>">Bảng điều khiển</a></li>

                    <li class="parent"><a href="#">Quản lý nội dung</a>
                        <ul>
                            <li><a href="<?php echo FCPATH; ?>fc-admin/post-new.php">Thêm bài viết</a></li>
                            <li><a href="<?php echo FCPATH; ?>fc-admin/edit.php">Quản lý bài viết</a></li>
                            <li><a href="<?php echo FCPATH; ?>fc-admin/categories.php">Quản lý chuyên mục</a></li>
                        </ul>
                    </li>

                    <li class="parent"><a href="#">Quản lý menu</a>
                        <ul>
                            <li><a href="<?php echo FCPATH; ?>fc-admin/menu.php">Quản lý menu chính</a></li>
                            <?php
                            foreach ($p_menu as $item) {
                                echo '<li><a href="' . FCPATH . 'fc-admin/cmenu.php?Itemid=' . $item->id . '">' . $item->name . '</a></li>';
                            }
                            ?>
                        </ul>
                    </li>

                    <li class="parent"><a href="#">Quản lý bình luận</a>
                        <ul>
                            <li><a href="<?php echo FCPATH; ?>fc-admin/comment.php">Tất cả bình luận</a></li>
                            <li><a href="<?php echo FCPATH; ?>fc-admin/comment.php?status=0">Bình luận chờ duyệt</a>
                            </li>
                        </ul>
                    </li>

                    <li class="parent"><a href="#">Cấu hình chung</a>
                        <ul>
                            <li><a href="#">Cấu hình website</a></li>
                            <li><a href="#">Quản lý hiển thị</a></li>
                        </ul>
                    </li>
                    <li><a href="#">Người sử dụng</a>
                        <ul>
                            <li><a href="<?php echo FCPATH; ?>fc-admin/profile.php">Trang cá nhân</a></li>
                            <li><a href="<?php echo FCPATH . '/fc-admin/login.php?action=logout' ?>">Đăng xuất</a></li>
                        </ul>
                    </li>
                <?php } else { ?>
                    <li class="parent"><a href="<?php echo FCPATH; ?>fc-admin/login.php"><img
                                    src="images/icon-48-user.png" alt="Đăng nhập" width="20" height="20"
                                    style="vertical-align: -4px;"/> &nbsp;Đăng nhập quản trị viên</a></li>
                    <li class="parent"><a href="<?php echo FCPATH; ?>fc-admin/lostpass.php"><img
                                    src="images/icon-48-user.png" alt="Đăng nhập" width="20" height="20"
                                    style="vertical-align: -4px;"/> &nbsp;Quên mật khẩu</a></li>
                <?php } ?>
            </ul>
        </div>
        <div class="menu">
            <ul class="menu-right">
                <li><a target="_blank" href="<?php echo FCPATH; ?>">Xem Web</a></li>
                <?php if ($FcSession->is_logged_in() == true) : ?>
                    <li><a href="<?php echo FCPATH . 'fc-admin/login.php?action=logout'; ?>">Đăng xuất</a></li>
                <?php endif; ?>
            </ul>
        </div>

    </div>
    