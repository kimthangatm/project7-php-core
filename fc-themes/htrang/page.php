<?php require_once($fc_themes . '/header.php'); ?>
<?php require_once($fc_themes . '/sidebar.php'); ?>
    <div class="content">
        <?php if ($fc_have_post) : ?>
            <?php foreach ($fc_posts as $post) { ?>
                <div class="post">
                    <img class="thumbnail" src="<?php echo $post->thumbnail_link ?>" width="120" height="120"
                         title="<?php echo $post->title; ?>" alt="<?php echo $post->title; ?>"/>
                    <h3><?php echo $post->title ?></h3>
                    <?php echo " " . $post->introl . $post->fulltext; ?>
                </div><!--End #post-->
            <?php } ?>
        <?php endif; ?>
    </div><!--End #content-->
<?php require_once($fc_themes . '/footer.php'); ?>