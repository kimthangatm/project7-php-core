﻿<div class="sidebar">
    <h4 class="top_side">ĐỊA ĐIỂM DU LỊCH</h4>
    <ul>
        <?php
        $db =& $DBO;
        $query = "select a.*, b.`name` as ten from `tbl_cmenu` as a left join `tbl_pmenu` as b on a.`id_pmenu` = b.`id`
                where b.`slug`='dia-diem-du-lich' order by `id` asc";
        $db->setQuery($query);
        $rows_side = $db->loadObjectLists();
        foreach ($rows_side as $row_side) {
            echo "<li><a href=" . $row_side->link . ">" . $row_side->name . "</a></li>";
        }
        ?>
    </ul>
    <h4 class="top_side">ẨM THỰC BỐN PHƯƠNG</h4>
    <ul>
        <?php
        $db =& $DBO;
        $query = "select a.* from `tbl_cmenu` as a left join `tbl_pmenu` as b on a.`id_pmenu` = b.`id`
                where b.`slug`='am-thuc-4-phuong' order by `id` asc";
        $db->setQuery($query);
        $rows_menu = $db->loadObjectLists();
        foreach ($rows_menu as $row_menu) {
            echo "<li><a href=" . $row_menu->link . ">" . $row_menu->name . "</a></li>";
        }
        ?>
    </ul>
    <h4 class="top_side">Khu Vực Ăn Chơi</h4>
    <ul>
        <?php
        $db =& $DBO;
        $query = "select a.* from `tbl_cmenu` as a left join `tbl_pmenu` as b on a.`id_pmenu` = b.`id`
                where b.`slug`='an-choi' order by `id` asc";
        $db->setQuery($query);
        $rows_menu = $db->loadObjectLists();
        foreach ($rows_menu as $row_menu) {
            echo "<li><a href=" . $row_menu->link . ">" . $row_menu->name . "</a></li>";
        }
        ?>
    </ul>
</div>