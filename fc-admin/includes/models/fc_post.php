<?php

class Fc_Post
{
    var $_db = null;
    var $_id = null;
    var $_beauty = null;
    var $_limitstart = null;
    var $_name = null;

    var $_posts = null;
    var $_paging = null;
    var $_have_post = null;

    function __construct($id = '', $name = '', $beauty = '', $limitstart = '1', &$db)
    {
        $this->_db = &$db;
        $this->_id = $id;
        $this->_beauty = $beauty;
        $this->_limitstart = $limitstart;
        $this->_name = $name;
        $this->__loadData();
    }

    function __amplink($link)
    {
        return str_replace('&', '&amp;', $link);
    }

    function __loadData()
    {

        $where = null;
        if ($this->_id != '') {
            if ($this->_name == 'cat') $where .= ' AND `id_cat`=' . $this->_id . ' ';
            if ($this->_name == 'author') $where .= ' AND `id_user`=' . $this->_id . ' ';
        }
        if ($this->_beauty != '') $where .= ' AND `beauty`=' . $this->_beauty . ' ';
        $query = "SELECT count(id) as 'nums' FROM `tbl_post` WHERE `publish` = 1 " . $where . " LIMIT 1";
        $this->_db->setQuery($query);
        //echo $this->_db->_sql;echo "-OK-";exit();
        $tmp = $this->_db->loadObject();
        //$limit : tổng số bản ghi
        $limit = $tmp->nums;
        //$path : đường link chính, không chứa các biến $_GET
        $path = FCPATH . 'index.php';
        //$display : số bản ghi hiển thị trên 1 trang
        $display = DISPLAY;

        if ($this->_limitstart <= 0 || $this->_limitstart > ceil($limit / $display)) {
            $this->_limitstart = 1;
        }

        if ($this->_name != '') {
            $params = array();
            $params[] = $this->_name . '=' . $this->_id;
        } else {
            $params = null;
            $params = '';
        }
        //echo $path.'---'.$this->_limitstart.'---'.$limit.'---'.$display;echo "-OK-";exit();
        $this->_paging = Helper::Paging($path, $this->_limitstart, $limit, $display, $params);

        $l1 = ($this->_limitstart - 1) * $display;

        $where = '';
        if ($this->_beauty != '') $where .= ' AND a.`beauty`=' . $this->_beauty . ' ';
        if ($this->_name == 'cat') $where .= ' AND a.`id_cat`=' . $this->_id . ' ';
        if ($this->_name == 'author') $where .= ' AND a.`id_user`=' . $this->_id . ' ';
        if ($this->_name == 'page' || $this->_name == 'p') $where .= ' AND a.`id`=' . $this->_id . ' ';

        $query = "SELECT a.*,b.`cat_name`,c.`user_name`,c.`display_name` FROM `tbl_post` AS a "
            . "LEFT JOIN `tbl_cat` AS b ON a.`id_cat`=b.`id` "
            . "LEFT JOIN `tbl_user` AS c ON a.`id_user`=c.`id` "
            . "WHERE a.`publish` = 1 " . $where . " ORDER BY a.`id` DESC LIMIT " . $l1 . ',' . $display;
        $this->_db->setQuery($query);
        $rows = $this->_db->loadObjectLists();
        //Mặc định tên biến là $Have_Post
        if (count($rows) > 0) {
            //Biến $Have_Post là mặc định, lúc nào cũng có.....
            $this->_have_post = true;
            //Mặc định tên biến là $Post, tất cả các trang đều như vậy......
            $Posts = array();
            foreach ($rows as $row) {
                //Khởi tạo biến $post;...cơ bản gồn những thứ này, còn nữa thì chưa thử vì làm nhanh.
                $post = (object)'';
                $post->id = $row->id;
                $post->title = $row->title;
                $post->slug = $row->slug_post;
                $post->thumbnail_link = $this->__amplink($row->thumbnail);
                $post->introl = $row->introl;
                $post->fulltext = $row->fulltext;
                $post->date = $row->created;

                $post->author = $row->display_name == '' ? $row->user_name : $row->display_name;
                $post->tag = $row->tag;
                $post->post_link = FCPATH . 'index.php?p=' . $row->id;
                $post->cat_link = FCPATH . 'index.php?cat=' . $row->id_cat;
                $post->cat_name = $row->cat_name;
                $post->author_link = FCPATH . 'index.php?author=' . $row->id_user;
                //Gán phần tử mảng tiếp theo = $post;
                $Posts[] = $post;
            }
            $this->_posts = $Posts;
        } else {
            $this->_have_post = false;
        }
        unset($rows, $Posts);
    }

    function getPaging()
    {
        return $this->_paging;
    }

    function getPost()
    {
        return $this->_posts;
    }

    function getHavePosts()
    {
        return $this->_have_post;
    }
}

?>