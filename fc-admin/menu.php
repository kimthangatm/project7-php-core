<?php require_once('loader.php'); ?>
<?php $sitename = "Quản lý menu chính| " . SITENAME ?>
<?php include('header.php') ?>
<?php
if ($FcSession->is_logged_in() == false) {
    Helper::redirect_to('login.php');
}
$db = &$DBO;
require_once('includes/models/tbl_pmenu.php');

if (isset($_POST['cid'])) {
    if (count($_POST['cid']) > 0) {
        $cid = $_POST['cid'];
        $Ob = new Tbl_PMenu($db);
        $array_error = array();
        $xoa_thanh_cong = 0;
        foreach ($cid as $id) {
            if (Helper::isNumeric($id)) {
                if ($Ob->delete($id)) {
                    $xoa_thanh_cong++;
                    $query = "DELETE FROM `tbl_cmenu` WHERE `id_pmenu` = " . $id;
                    $db->setQuery($query);
                    $db->query();
                }
            } else {
                $array_error[] = $id;
            }
        }
    }
}

if (isset($_POST['submit'])) {
    $data = Helper::trimData($_POST);
    $data['name'] = $db->escape_value($data['name']);
    $data['description'] = $data['description'];
    if ($data['slug'] == '') {
        $data['slug'] = Helper::slugVN($data['name']);
    }
    if ($data['name'] != '') {
        $ObjectPmenu = new Tbl_PMenu($db);
        if ($ObjectPmenu->store($data)) {
            if ($data['id'] == null)
                $not_error_data = "Menu <span style='color:blue;'>" . $data['name'] . "</span> đã được tạo. Vui lòng tải lại trang !!!";
            else $not_error_data = "Menu <span style='color:blue;'>" . $data['name'] . "</span> đã được thay đổi.";
            $error_data = null;
        } else {
            $not_error_data = "Lỗi tạo menu do <span style='color:red;'> Tên viết tắt bị trùng lặp</span>. Vui lòng xem lại!";
        }
    } else {
        $error_data = "Tên menu";
    }
} else {
    $data_edit = $_GET;
    if ($data_edit['action'] == 'edit' && Helper::isNumeric($data_edit['mid'])) {
        $sql = "SELECT * FROM `tbl_pmenu` WHERE `id`=" . $data_edit['mid'];
        $db->setQuery($sql);
        $row_edit = $db->loadObject();
        $viewEdit = true;
    }
}

//Lấy dữ liệu từ tbl_pmenu  
$query = "SELECT * FROM `tbl_pmenu`";
$db->setQuery($query);
$rows = $db->loadObjectLists();
?>
    <div class="content">
        <fieldset style="padding: 0;">
            <table style="width: 100%;">
                <tr>
                    <td><h2 id="menu-php" class="phplogo">Quản lý menu chính</h2></td>
                    <td align="right">
                        <div class="tool-bar">
                            <table style="padding: 0px;" align="right">
                                <tr align="center">
                                    <td><input type="button" value="" name="remove-trash" class="remove-trash"
                                               onclick="document.detalscategories.submit();"/></td>
                                </tr>
                                <tr align="center" style="font-size: 12px;">
                                    <td>Xóa menu</td>
                                </tr>
                            </table>
                        </div>
                    </td>
                </tr>
            </table>
        </fieldset>
        <?php if (isset($error_data) || isset($not_error_data) || isset($array_error) || $xoa_thanh_cong > 0): ?>
            <fieldset>
                <table>
                    <tr>
                        <td colspan="2">
                            <ul>
                                <?php
                                if ($xoa_thanh_cong > 0)
                                    echo $xoa_thanh_cong ? "<li>Xóa thành công <span style='color:blue;'>" . $xoa_thanh_cong . " </span> menu! </li>" : '';
                                if (isset($array_error)) {
                                    foreach ($array_error as $aerror) {
                                        echo "<li>Xóa menu số<span style='color:red'> " . $aerror . "</span> không thành công</li>";
                                    }
                                }
                                echo $error_data ? "<li>Trường <span style='color:blue;'>" . $error_data . "</span> để trống.</li>" : '';
                                if (isset($not_error_data))
                                    echo $not_error_data ? "<li><h3>" . $not_error_data . "</h3></li>" : '';
                                ?>
                            </ul>
                        </td>

                    </tr>
                </table>
            </fieldset>
        <?php endif; ?>
        <?php if ($viewEdit) { ?>
            <form method="post" action="<?php echo FCPATH . 'fc-admin/menu.php' ?>" name="editmenu">
                <fieldset style="float: left; width: 45%;">
                    <table width="100%">
                        <tr>
                            <td width="100">Tên menu</td>
                            <td><input style="width: 95%;" type="text" name="name" class="ftext"
                                       value="<?php echo $row_edit->name; ?>"/></td>
                        </tr>

                        <tr>
                            <td width="100">Tên viết tắt</td>
                            <td><input style="width: 95%;" type="text" name="slug" class="ftext"
                                       value="<?php echo $row_edit->slug; ?>"/></td>
                        </tr>
                        <tr>
                            <td width="100" valign="top">Mô tả</td>
                            <td><textarea style="width: 96%;" cols="10" rows="10"
                                          name="description"><?php echo $row_edit->description; ?></textarea></td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td align="left">
                                <input type="submit" class="search"
                                       style="padding:5px; margin-left: 0; margin-top: 10px;" value="Lưu chỉnh sửa"
                                       name="submit"/>
                                <a href="<?php echo FCPATH . 'fc-admin/menu.php'; ?>"><input type="button"
                                                                                             name="button1"
                                                                                             style="padding:5px; margin-left: 0; margin-top: 10px;"
                                                                                             value="Hủy chỉnh sửa"
                                                                                             class="search"/></a>
                                <input type="hidden" name="id" value="<?php echo $row_edit->id; ?>"/>
                            </td>
                        </tr>
                    </table>
                </fieldset>
            </form>
        <?php } else { ?>
            <form method="post" action="<?php echo FCPATH . 'fc-admin/menu.php' ?>" name="addnewmenu">
                <fieldset style="float: left; width: 45%;">
                    <table width="100%">
                        <tr>
                            <td width="100">Tên menu</td>
                            <td><input style="width: 95%;" type="text" name="name" class="ftext" value=""/></td>
                        </tr>

                        <tr>
                            <td width="100">Tên viết tắt</td>
                            <td><input style="width: 95%;" type="text" name="slug" class="ftext" value=""/></td>
                        </tr>
                        <tr>
                            <td width="100" valign="top">Mô tả</td>
                            <td><textarea style="width: 96%;" rows="10" cols="10" name="description"></textarea></td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td align="left">
                                <input type="submit" class="search"
                                       style="padding:5px; margin-left: 0; margin-top: 10px;" value="Tạo menu"
                                       name="submit"/>
                                <a href="<?php echo FCPATH . 'fc-admin/menu.php'; ?>"><input type="button"
                                                                                             name="button1"
                                                                                             style="padding:5px; margin-left: 0; margin-top: 10px;"
                                                                                             value="Hủy"
                                                                                             class="search"/></a>
                                <input type="hidden" name="id" value=""/>
                            </td>
                        </tr>
                    </table>
                </fieldset>
            </form>
        <?php } ?>
        <form method="post" action="<?php echo FCPATH . 'fc-admin/menu.php' ?>" name="detalscategories">
            <fieldset style="float: right;">
                <table class="categories-php">
                    <thead>
                    <tr>
                        <th width="10">STT</th>
                        <th width="20">#</th>
                        <th width="160">Tên menu</th>
                        <th width="60">Chi tiết</th>
                        <th width="160">Tên viết tắt <span style="color: red;">(*)</span></th>
                        <th width="40">ID</th>
                    </tr>
                    </thead>

                    <?php $i = 0;
                    foreach ($rows as $row) {
                        $i++; ?>
                        <tr>
                            <td><?php echo $i; ?></td>
                            <td><input type="checkbox" name="cid[]" value="<?php echo $row->id; ?>"/></td>
                            <td align="left" style="padding-left: 5px;"><a
                                        href="<?php echo FCPATH . 'fc-admin/menu.php?action=edit&amp;mid=' . $row->id; ?>"><?php echo $row->name ?></a>
                            </td>
                            <td><a href="<?php echo FCPATH . 'fc-admin/cmenu.php?Itemid=' . $row->id; ?>"><img
                                            src="images/icon-16-help-docs.png" alt="" title="Chi tiết menu"/></a></td>
                            <td align="left" style="padding-left: 5px;"><?php echo $row->slug; ?></td>
                            <td><?php echo $row->id; ?></td>
                        </tr>
                    <?php } ?>
                    <tfoot>
                    <tr>
                        <td colspan="6"><span style="color: red;">(*)</span> Tên viết tắt không trùng nhau</td>
                    </tr>
                    </tfoot>
                </table>
            </fieldset>
        </form>
    </div>
    <div class="clear"></div>
<?php include('footer.php') ?>