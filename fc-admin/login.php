<?php require_once('loader.php'); ?>
<?php
if (isset($_GET['action'])) {
    if ($_GET['action'] == 'logout') {
        $FcSession->logout();
        Helper::redirect_to('login.php');
    }
}

if ($FcSession->is_logged_in() == true) {
    Helper::redirect_to('index.php');
}
?>
<?php $sitename = "Đăng nhập | " . SITENAME ?>
<?php include('header.php') ?>
<?php
if (isset($_POST['submit'])) {
    $array1 = array('user_name', 'user_pass');
    $array2 = array('Tên đăng nhập', 'Mật khẩu');
    $array_error = Helper::checkField($array1, $array2);
    if (count($array_error) == 0) {
        $db = &$DBO;
        $user = $db->escape_value(trim($_POST['user_name']));
        $pass = $db->escape_value(trim($_POST['user_pass']));
        if (!Helper::checkPass($pass) || !Helper::checkUser($user) || strlen($user) < 4 || strlen($pass) < 4) {
            $FcSession->message = "Đăng nhập thất bại!";
            Helper::redirect_to('login.php');
        }
        require_once('includes/models/tbl_user.php');
        $user_login = new Tbl_User($db);
        $user_login_ok = $user_login->checkLogin($user, $pass);
        if ($user_login_ok == false) {
            $FcSession->message = "Đăng nhập thất bại!";
        } else {
            $FcSession->login($user_login_ok);
            Helper::redirect_to('index.php');
        }
    }
}
?>
<div class="content">
    <form method="post" action="login.php" name="loginForm">
        <fieldset style="box-shadow: 2px 2px 2px 2px rgba(0, 0, 0, 0.2);">
            <table id="loginFrom">
                <?php if (!empty($array_error) || isset($FcSession->message)) { ?>
                    <tr>
                        <td colspan="2" align="center">
                            <ul>
                                <?php
                                foreach ($array_error as $error) {
                                    echo "<li>Trường <span style='color:red;'>" . $error . "</span> bị lỗi.</li>";
                                }
                                if (isset($FcSession->message)) {
                                    echo "<li>Thông báo:<span style='color:red;'>" . $FcSession->message . "</span></li>";
                                }
                                ?>
                            </ul>
                        </td>

                    </tr>
                <?php } ?>
                <tr>
                    <td colspan="2" align="center"><h1 style="padding-bottom: 10px;">Landscape Travel</h1></td>
                </tr>
                <tr>
                    <td>Tên đăng nhập:</td>
                    <td><input type="text" class="ftext" name="user_name" value="" maxlength="30"/></td>
                </tr>
                <tr>
                    <td>Mật khẩu:</td>
                    <td><input type="password" class="ftext" name="user_pass" autocomplete="off" value=""
                               maxlength="32"/></td>
                </tr>
                <tr>
                    <td align="center" colspan="2"><input type="submit" class="fsubmit" name="submit"
                                                          value="Đăng nhập"/></td>
                </tr>
            </table>
        </fieldset>
    </form>
</div>
<?php include('footer.php') ?>    